# Game of Life

A quick implementation of the game of life in Javascript.

# Live demo

[http://cerisier.github.io/game-of-life/](http://cerisier.github.io/game-of-life/)

# Screenshot

![screenshot 2014-12-28 17 57 38 2](https://cloud.githubusercontent.com/assets/1126594/5564385/68e933d0-8ebc-11e4-9436-da6ce17e03dc.png)

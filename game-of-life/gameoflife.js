var GameOfLife = function(board) {

  var legacy = null;

  function getCellSurroundCount(grid, x, y) {
    var count = 0;

    count += grid[y - 1][x - 1];
    count += grid[y][x - 1];
    count += grid[y + 1][x - 1];
    count += grid[y + 1][x];
    count += grid[y + 1][x + 1];
    count += grid[y][x + 1];
    count += grid[y - 1][x + 1];
    count += grid[y - 1][x];

    return count;
  }

  function generate() {

    legacy = new Array(board.rows);
    for (var i = 0; i < board.rows; i++) {
      legacy[i] = new Array(board.columns);
      for (var j = 0; j < board.columns; j++) {
        legacy[i][j] = Math.round(Math.random())
      }
    }
  }

  function getNextGeneration() {

    var nextGen = new Array(board.rows);
    for (var y = 0; y < board.rows; y++) {
      nextGen[y] = new Array(board.columns);
    }

    for (var y = 1; y < board.rows - 1; y++) {
      for (var x = 1; x < board.columns - 1; x++) {

        var cellCount = getCellSurroundCount(legacy, x, y);

        if (legacy[y][x] == 1) {
          if (cellCount == 2 || cellCount == 3)
            nextGen[y][x] = 1;
          else
            nextGen[y][x] = 0;
        }
        else {
          if (cellCount == 3)
            nextGen[y][x] = 1;
          else
            nextGen[y][x] = 0;
        }
      }
    }

    legacy = nextGen;
    return nextGen;
  }

  return {
    generate: generate,
    getNextGeneration: getNextGeneration
  };
};

var Board = function(height, width, cellSize, borderWidth) {

  var canvas = document.getElementById('scene');
  var ctx = canvas.getContext('2d');

  ctx.fillStyle = "rgb(200,0,0)";

  canvas.height = height;
  canvas.width = width;

  var rows = Math.floor(height / (cellSize + borderWidth));
  var columns = Math.floor(width / (cellSize + borderWidth));

  function display(grid) {

    ctx.clearRect(0, 0, width, height);
    ctx.fillStyle = "rgb(130,200,90)";

    for (var y = 1; y < rows - 1; y++) {
      for (var x = 1; x < columns - 1; x++) {
        if (grid[y][x] == 1)
          ctx.fillRect(x * cellSize + x * borderWidth, y * cellSize + y * borderWidth, cellSize, cellSize);
      }
    }
  }

  return {
    rows: rows,
    columns: columns,
    display: display
  };
};

var GameLoop = function() {

  var board = Board(window.innerHeight, window.innerWidth, 4, 1);
  var game  = GameOfLife(board);

  var fps = 60;
  var frameDuration = 1000/fps;
  var then = 0;
  var delta;

  game.generate();

  function loop(now) {

    delta = now - then;
    if (delta > frameDuration) {

      var gen = game.getNextGeneration();
      board.display(gen);

      then = now - (delta % frameDuration);
    }
    requestAnimationFrame(loop);
  }

  requestAnimationFrame(loop);
}

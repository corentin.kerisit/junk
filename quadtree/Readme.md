# QuadTree

A very basic implementation of a QuadTree.<br>
Only support simple Point elements for now.

# Demo

A quick demo is available here:

http://cerisier.github.io/quadtree/

Or follow the instructions below:
```
git clone https://github.com/cerisier/quadtree
cd quadtree
npm install -g gulp
npm install
gulp
cd examples && python -m SimpleHTTPServer
```

# Screenshot

![screenshot 2015-01-02 03 47 50](https://cloud.githubusercontent.com/assets/1126594/5593971/29e8b844-9232-11e4-8592-b62687900c5c.png)

# TODO

- Add tests
- Add multiple type of elements handling (Rectangles, Square, Triangles, Lines, etc.)
- Add static server to gulp tasks

var QuadTree = require('../').QuadTree;
var Box = require('../').Box;

var zones = ['nw', 'ne', 'sw', 'se'];

var canvas;
var ctx;
var tree;
var points = [];

function genRandomBetween(min, max, canBeZero) {
  var rand;
  do {
    rand = Math.random() * (max - min) + min;
  }
  while(!canBeZero && Math.round(rand) == 0);

  return rand;
}

function drawNode(node) {

  var box = node.getZone();
  var subZones = node.getSubZones();

  ctx.rect(box.x, box.y, box.height, box.width);
  ctx.stroke();

  if (subZones['nw']) {
    drawNode(subZones['nw'], ctx);
    drawNode(subZones['ne'], ctx);
    drawNode(subZones['sw'], ctx);
    drawNode(subZones['se'], ctx);
  }

}

function update(timestamp) {

  ctx.clearRect(0, 0, 1000, 1000);

  var items = points;

  tree.clear()
  //tree = new QuadTree(0, new Box(0, 0, 1000, 1000));

  var len = items.length;

  for (var i = 0; i < len; i++) {
    var item = items[i];

    item.isColliding = false;

    item.x = item.x + item.sx;
    item.y = item.y + item.sy;

    if (item.x > canvas.width) {
      item.x = canvas.width;
      item.sx = item.sx * -1;
    }
    else if (item.x < 0) {
      item.x = 0;
      item.sx = item.sx * -1;
    }

    if (item.y > canvas.height) {
      item.y = canvas.height;
      item.sy = item.sy * -1;
    }
    else if (item.y < 0) {
      item.y = 0;
      item.sy = item.sy * -1;
    }

    tree.insert(item);
  }

  var items = points;
  var len = items.length;

  for (var i = 0; i < len; i++) {

    var item = items[i];
    var neighbors = tree.getNeighborhood(item);

    for (var j = 0, nlen = neighbors.length; j < nlen; j++) {

      var neighbor = neighbors[j];

      if (neighbor != item) {
        var dx = item.x - neighbor.x;
        var dy = item.y - neighbor.y;
        var radii = 10 + 10; // 2 * radius

        if (item.isColliding && neighbor.isColliding)
          continue;

        var colliding = ((dx * dx) + (dy * dy)) < (radii * radii);

        // don't update colliding state to non colliding neighbors
        // only to colliding elements
        if (colliding) {
          item.isColliding = colliding;
          neighbor.isColliding = colliding;
        }

      }
    }

    if (item.isColliding) {
      ctx.fillStyle = 'rgb(0, 0, 200)';
    }
    else {
      ctx.fillStyle = 'rgb(200, 0, 0)';
    }
    ctx.beginPath();
    ctx.arc(item.x,item.y,10,0,2*Math.PI);
    ctx.closePath();
    ctx.fill();
  }

  drawNode(tree);

  requestAnimationFrame(update);
}

function initWorld() {

  var bounds = new Box(0, 0, canvas.height, canvas.width);
  var maxSpeed = 2;

  tree = new QuadTree(0, bounds);

  for (var i = 0; i < 100; i++) {
    var x = Math.round(Math.random() * bounds.width);
    var y = Math.round(Math.random() * bounds.height);

    var sx = genRandomBetween(-maxSpeed, maxSpeed, false);
    var sy = genRandomBetween(-maxSpeed, maxSpeed, false);

    var point = {x: x, y: y, sx: sx, sy: sy};

    points.push(point);
    tree.insert(point);
  }
}

document.addEventListener("DOMContentLoaded", function(event) {

  canvas = document.getElementById('canvas');
  ctx = canvas.getContext('2d');

  ctx.fillStyle = 'rgb(200, 0, 0)';

  initWorld();

  requestAnimationFrame(update);

});

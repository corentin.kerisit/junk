var gulp = require('gulp');
var browserify = require('browserify');
var transform = require('vinyl-transform');

gulp.task('browserify', function () {
  var browserified = transform(function(filename) {
    var b = browserify(filename);
    return b.bundle();
  });

  return gulp.src(['./examples/collision.js'])
  .pipe(browserified)
  .pipe(gulp.dest('./examples/.build'));
});

gulp.task('default', ['browserify']);

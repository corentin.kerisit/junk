"use strict";

var MAX_ELEMENTS = 5;

function Box(x, y, height, width) {
  return {
      x: x,
      y: y,
      height: height,
      width: width
  };
}

function QuadTree(level, zone) {

    var _level = level;
    var _zone = zone;
    var _objects = [];
    var _subZones = {
      nw: null,
      ne: null,
      sw: null,
      se: null
    };

    function getTargetZone(item) {
      var zoneCenterX = _zone.x + _zone.width / 2;
      var zoneCenterY = _zone.y + _zone.height / 2;

      var north = item.y < zoneCenterY;
      var west = item.x < zoneCenterX;

      if (north) {
        if (!west)
          return 'ne';
        else
          return 'nw';
      }
      else {
        if (!west)
          return 'se';
        else
          return 'sw';
      }

      return 'lol';
    }

    function insert(object) {

      // If has subZones
      if (_subZones.nw != null) {
        var index = getTargetZone(object);
        if (index != null) {
          _subZones[index].insert(object);
          return;
        }
      }

      _objects.push(object);

      var len = _objects.length;

      if (len > MAX_ELEMENTS) {

        if (_subZones.nw == null) {
          split();
        }

        var obj;
        while (obj = _objects.pop()) {
          var index = getTargetZone(obj);
          if (index != null) {
            _subZones[index].insert(obj);
          }
        }
      }
    }

    function split() {

      var subZoneHeight = Math.floor(_zone.height / 2);
      var subZoneWidth = Math.floor(_zone.width / 2);
      var x = _zone.x;
      var y = _zone.y;

      _subZones.nw = QuadTree(_level + 1, new Box(x, y, subZoneHeight, subZoneWidth));
      _subZones.ne = QuadTree(_level + 1, new Box(x + subZoneWidth, y, subZoneHeight, subZoneWidth));
      _subZones.sw = QuadTree(_level + 1, new Box(x, y + subZoneWidth, subZoneHeight, subZoneWidth));
      _subZones.se = QuadTree(_level + 1, new Box(x + subZoneHeight, y + subZoneWidth,  subZoneHeight, subZoneWidth));
    }

    function getNeighborhood(item) {
        if (_subZones.nw) {
          var zone = getTargetZone(item);
          return _subZones[zone].getNeighborhood(item);
        }

        return _objects;
    }

    function clear() {
      _objects.length = 0;

      if (_subZones.nw) {
        _subZones.nw.clear();
        _subZones.ne.clear();
        _subZones.sw.clear();
        _subZones.se.clear();
      }

      _subZones = {nw: null, ne: null, sw: null, se: null};
    }

    return {
      getZone: function() { return _zone; },
      getSubZones: function() { return _subZones; },
      getNeighborhood: getNeighborhood,
      clear: clear,
      insert: insert
    };

}

module.exports = {
  QuadTree: QuadTree,
  Box: Box
};
